package
{
   import §ZuO\\§.§f$\n8\x01§;
   import flash.utils.describeType;
   import flash.utils.getDefinitionByName;
   import flash.utils.getQualifiedClassName;
   import flash.utils.getQualifiedSuperclassName;
   
   public class §%lF*\x01§
   {
       
      
      public function §%lF*\x01§()
      {
      }
      
      public static function §\x1370i\x02§(param1:Object) : Class
      {
         var _loc2_:String = getQualifiedClassName(param1);
         if(_loc2_ == "null" || _loc2_ == "Object" || _loc2_ == "int" || _loc2_ == "Number" || _loc2_ == "Boolean")
         {
            return null;
         }
         if(param1.hasOwnProperty("prototype"))
         {
            return null;
         }
         var _loc3_:* = getDefinitionByName(_loc2_) as Class;
         if(_loc3_.__isenum)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §\x1f\x0f\r?\x03§(param1:Object) : Class
      {
         var _loc2_:String = getQualifiedClassName(param1);
         if(_loc2_ == "null" || _loc2_.substr(0,8) == "builtin.")
         {
            return null;
         }
         if(param1.hasOwnProperty("prototype"))
         {
            return null;
         }
         var _loc3_:* = getDefinitionByName(_loc2_) as Class;
         if(!_loc3_.__isenum)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §'HRl\x01§(param1:Class) : Class
      {
         var _loc2_:String = getQualifiedSuperclassName(param1);
         if(_loc2_ == null || _loc2_ == "Object")
         {
            return null;
         }
         return getDefinitionByName(_loc2_) as Class;
      }
      
      public static function §\nzv\t\x01§(param1:Class) : String
      {
         if(param1 == null)
         {
            return null;
         }
         var _loc2_:String = getQualifiedClassName(param1);
         var _loc3_:String = _loc2_;
         if(_loc3_ == "int")
         {
            return "Int";
         }
         if(_loc3_ == "Number")
         {
            return "Float";
         }
         if(_loc3_ == "Boolean")
         {
            return "Bool";
         }
         return _loc2_.split("::").join(".");
      }
      
      public static function §)DU;\x01§(param1:Class) : String
      {
         return §%lF*\x01§.§\nzv\t\x01§(param1);
      }
      
      public static function §)u^\x02§(param1:String) : Class
      {
         var _loc3_:* = null as Class;
         var _loc5_:* = null as String;
         try
         {
            _loc3_ = getDefinitionByName(param1) as Class;
            if(_loc3_.__isenum)
            {
               return null;
            }
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            _loc5_ = param1;
            if(_loc5_ == "Int")
            {
               return int;
            }
            if(_loc5_ == "Float")
            {
               return Number;
            }
            return null;
         }
         if(_loc3_ == null || _loc3_.__name__ == null)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §J7?-\x01§(param1:String) : Class
      {
         var _loc3_:* = null;
         try
         {
            _loc3_ = getDefinitionByName(param1);
            if(!_loc3_.__isenum)
            {
               return null;
            }
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            if(param1 == "Bool")
            {
               return Boolean;
            }
            return null;
         }
         if(_loc3_ == null || _loc3_.__ename__ == null)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §N\x04$X§(param1:Class, param2:Array) : Object
      {
         switch(int(param2.length))
         {
            case 0:
               §§push(new param1());
               break;
            case 1:
               §§push(new param1(param2[0]));
               break;
            case 2:
               §§push(new param1(param2[0],param2[1]));
               break;
            case 3:
               §§push(new param1(param2[0],param2[1],param2[2]));
               break;
            case 4:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3]));
               break;
            case 5:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4]));
               break;
            case 6:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5]));
               break;
            case 7:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6]));
               break;
            case 8:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7]));
               break;
            case 9:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8]));
               break;
            case 10:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9]));
               break;
            case 11:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10]));
               break;
            case 12:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11]));
               break;
            case 13:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11],param2[12]));
               break;
            case 14:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11],param2[12],param2[13]));
         }
         return §§pop();
      }
      
      public static function §5|X\x04\x03§(param1:Class) : Object
      {
         var _loc3_:* = null as Object;
         try
         {
            §f$\n8\x01§.§\x1c\x14'\x01§ = true;
            _loc3_ = new param1();
            §f$\n8\x01§.§\x1c\x14'\x01§ = false;
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            §f$\n8\x01§.§\x1c\x14'\x01§ = false;
            §f$\n8\x01§.§+\x10gC§ = new Error();
            throw null;
         }
         return null;
      }
      
      public static function §T&1;\x01§(param1:Class, param2:String, param3:Array = undefined) : Object
      {
         var _loc4_:Object = §\x06SA\x06\x02§.GLWA(param1,param2);
         if(_loc4_ == null)
         {
            §f$\n8\x01§.§+\x10gC§ = new Error();
            throw "No such constructor " + param2;
         }
         if(§\x06SA\x06\x02§.§\x02/V6\x02§(_loc4_))
         {
            if(param3 == null)
            {
               §f$\n8\x01§.§+\x10gC§ = new Error();
               throw "Constructor " + param2 + " need parameters";
            }
            return _loc4_.apply(param1,param3);
         }
         if(param3 != null && int(param3.length) != 0)
         {
            §f$\n8\x01§.§+\x10gC§ = new Error();
            throw "Constructor " + param2 + " does not need parameters";
         }
         return _loc4_;
      }
      
      public static function §?\x11?\x12\x02§(param1:Class, param2:int, param3:Array = undefined) : Object
      {
         var _loc4_:String = param1.__constructs__[param2];
         if(_loc4_ == null)
         {
            §f$\n8\x01§.§+\x10gC§ = new Error();
            throw param2 + " is not a valid enum constructor index";
         }
         return §%lF*\x01§.§T&1;\x01§(param1,_loc4_,param3);
      }
      
      public static function §\x1edcZ\x03§(param1:*, param2:Boolean) : Array
      {
         var _loc8_:int = 0;
         var _loc3_:Array = [];
         var _loc4_:XML = describeType(param1);
         if(param2)
         {
            _loc4_ = _loc4_.factory[0];
         }
         var _loc5_:XMLList = _loc4_.child("method");
         var _loc6_:int = 0;
         var _loc7_:int = int(_loc5_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§\\0F\x01§.§E\b\x19\x0b§(_loc5_[_loc8_].attribute("name")));
         }
         var _loc9_:XMLList = _loc4_.child("variable");
         _loc6_ = 0;
         _loc7_ = int(_loc9_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§\\0F\x01§.§E\b\x19\x0b§(_loc9_[_loc8_].attribute("name")));
         }
         var _loc10_:XMLList = _loc4_.child("accessor");
         _loc6_ = 0;
         _loc7_ = int(_loc10_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§\\0F\x01§.§E\b\x19\x0b§(_loc10_[_loc8_].attribute("name")));
         }
         return _loc3_;
      }
      
      public static function §)Rl\b\x02§(param1:Class) : Array
      {
         return §%lF*\x01§.§\x1edcZ\x03§(param1,true);
      }
      
      public static function §\x01q\nY\x03§(param1:Class) : Array
      {
         var _loc2_:Array = §%lF*\x01§.§\x1edcZ\x03§(param1,false);
         _loc2_.§qdAn\x03§("__construct__");
         _loc2_.§qdAn\x03§("prototype");
         return _loc2_;
      }
      
      public static function §Hg^W§(param1:Class) : Array
      {
         var _loc2_:Array = param1.__constructs__;
         return _loc2_.§\x1eF(q\x01§();
      }
      
      public static function §hJo\n\x02§(param1:*) : §\x13\r\x05\x15\x01§
      {
         var _loc5_:* = null;
         var _loc3_:String = getQualifiedClassName(param1);
         var _loc4_:String = _loc3_;
         if(_loc4_ == "null")
         {
            return §\x13\r\x05\x15\x01§.§;V1\x03\x01§;
         }
         if(_loc4_ == "void")
         {
            return §\x13\r\x05\x15\x01§.§;V1\x03\x01§;
         }
         if(_loc4_ == "int")
         {
            return §\x13\r\x05\x15\x01§.§\x15Kd3\x01§;
         }
         if(_loc4_ == "Number")
         {
            if((param1 < -268435456 || param1 >= 268435456) && int(param1) == param1)
            {
               return §\x13\r\x05\x15\x01§.§\x15Kd3\x01§;
            }
            return §\x13\r\x05\x15\x01§.§-\x17d\x1a\x01§;
         }
         if(_loc4_ == "Boolean")
         {
            return §\x13\r\x05\x15\x01§.§\nxB{§;
         }
         if(_loc4_ == "Object")
         {
            return §\x13\r\x05\x15\x01§.§\t\x04to\x01§;
         }
         if(_loc4_ == "Function")
         {
            return §\x13\r\x05\x15\x01§.§J\x04\x19S\x02§;
         }
         _loc5_ = null;
         try
         {
            _loc5_ = getDefinitionByName(_loc3_);
            if(param1.hasOwnProperty("prototype"))
            {
               return §\x13\r\x05\x15\x01§.§\t\x04to\x01§;
            }
            if(_loc5_.__isenum)
            {
               return §\x13\r\x05\x15\x01§.§Y`WA§(_loc5_);
            }
            return §\x13\r\x05\x15\x01§.§\x1d\x19h)\x02§(_loc5_);
         }
         catch(_loc6_:*)
         {
            if(null as Error)
            {
               §f$\n8\x01§.§+\x10gC§ = null;
            }
            if(_loc3_ == "builtin.as$0::MethodClosure" || int(_loc3_.indexOf("-")) != -1)
            {
               return §\x13\r\x05\x15\x01§.§J\x04\x19S\x02§;
            }
            return _loc5_ == null?§\x13\r\x05\x15\x01§.§J\x04\x19S\x02§:§\x13\r\x05\x15\x01§.§\x1d\x19h)\x02§(_loc5_);
         }
         return null;
      }
      
      public static function §?\x14E\x18\x01§(param1:Object, param2:Object) : Boolean
      {
         var _loc4_:* = null as Array;
         var _loc5_:* = null as Array;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         if(param1 == param2)
         {
            return true;
         }
         try
         {
            if(param1.§5\x04\x10[\x02§ != param2.§5\x04\x10[\x02§)
            {
               return false;
            }
            _loc4_ = param1.§N\nM\x03§;
            _loc5_ = param2.§N\nM\x03§;
            _loc6_ = 0;
            _loc7_ = int(_loc4_.length);
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               if(!§%lF*\x01§.§?\x14E\x18\x01§(_loc4_[_loc8_],_loc5_[_loc8_]))
               {
                  return false;
               }
            }
         }
         catch(_loc9_:*)
         {
            return false;
         }
         return true;
      }
      
      public static function §;\n\x12w\x03§(param1:Object) : String
      {
         return param1.§ejh\x01§;
      }
      
      public static function §D\x1b5&§(param1:Object) : Array
      {
         return param1.§N\nM\x03§ == null?[]:param1.§N\nM\x03§;
      }
      
      public static function §\x16\x01M\x03§(param1:Object) : int
      {
         return param1.§5\x04\x10[\x02§;
      }
      
      public static function §\x1e\x15\r{\x02§(param1:Class) : Array
      {
         var _loc5_:* = null as String;
         var _loc6_:* = null as Object;
         var _loc2_:Array = [];
         var _loc3_:Array = param1.__constructs__;
         var _loc4_:int = 0;
         while(_loc4_ < int(_loc3_.length))
         {
            _loc5_ = _loc3_[_loc4_];
            _loc4_++;
            _loc6_ = §\x06SA\x06\x02§.GLWA(param1,_loc5_);
            if(!§\x06SA\x06\x02§.§\x02/V6\x02§(_loc6_))
            {
               _loc2_.push(_loc6_);
            }
         }
         return _loc2_;
      }
   }
}
