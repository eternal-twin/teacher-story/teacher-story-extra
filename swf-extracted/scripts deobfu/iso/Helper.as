package iso
{
   import flash.Boot;
   import flash.display.MovieClip;
   import haxe.Log;
   import mt.deepnight.§\x13\x1eDd§;
   
   public class Helper extends §/2?§
   {
       
      
      public var §\x1bJV%§:_Helper;
      
      public var §R\x12§:MovieClip;
      
      public var §N(Uu\x01§:Object;
      
      public var §Q8\x05;\x02§:Object;
      
      public var §;H1\x03§:String;
      
      public var §T\x10gT§:Boolean;
      
      public function Helper(param1:_Helper = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         super();
         §\x1bJV%§ = param1;
         §T\x10gT§ = false;
         §}$2V\x02§ = false;
         §Xj(/§ = 0.15;
         §\to=\x1a\x02§ = §\to=\x1a\x02§ * 0.5;
         §\x1b?\x02E\x01§(true);
         §`Y@S\x03§(null,int(§*n8N\x03§.§\x1f@\x071§.x) - 3,int(§*n8N\x03§.§\x1f@\x071§.§j\x01§) + 12);
         §Q8\x05;\x02§ = {
            "MV":int(§*n8N\x03§.§\x1f@\x071§.x) + 1,
            "j\x16":int(§*n8N\x03§.§\x1f@\x071§.§j\x01§),
            "Oxf":1
         };
         §sn3%\x03§ = true;
         §N(Uu\x01§ = {
            "x":0,
            "j\x01":12
         };
      }
      
      override public function §Q]L?\x01§(param1:int, param2:int) : void
      {
         super.§Q]L?\x01§(param1,param2);
         if(§;H1\x03§ == "walk" && (param1 < 0 || param2 < 0))
         {
            §i`2g§("back");
         }
         if(§;H1\x03§ == "back" && (param1 > 0 || param2 > 0))
         {
            §i`2g§("walk");
         }
      }
      
      public function §i`2g§(param1:String = undefined) : void
      {
         var _loc3_:* = null as MovieClip;
         if(param1 == null)
         {
            param1 = "stand";
         }
         §R\x12§.gotoAndStop(param1);
         §;H1\x03§ = param1;
         try
         {
            _loc3_ = Reflect.field(§R\x12§._sub,"_sub");
            _loc3_.stop();
            return;
         }
         catch(_loc4_:*)
         {
            return;
         }
      }
      
      override public function §\x0eCma\x02§() : void
      {
         super.§\x0eCma\x02§();
         §i`2g§("stand");
         §y+§.§t#§.§\n;2h\x01§("helperMove");
      }
      
      public function §wYf\x06\x01§() : Boolean
      {
         return §;H1\x03§ != "stand";
      }
      
      public function §o;\x11;\x01§() : void
      {
         var _g:Helper = this;
         §\x01\x17\x0fL\x02§();
         §T\x10gT§ = false;
         §y+§.§t#§.__beginNewQueue();
         §y+§.§t#§.__add(function():void
         {
            _g.§Er\x133§(§*n8N\x03§.§\x1f@\x071§);
         },0);
         §y+§.§t#§.__add(function():void
         {
            null;
         },0,"helperMove");
         §y+§.§t#§.__add(function():void
         {
            _g.§\tTU(\x02§(false);
         },0);
         §y+§.§t#§.__add(function():void
         {
            300;
         },300);
         §y+§.§t#§.__add(function():void
         {
            _g.§`Y@S\x03§(null,int(§*n8N\x03§.§\x1f@\x071§.x) - 2,int(§*n8N\x03§.§\x1f@\x071§.§j\x01§));
         },0);
         §y+§.§t#§.__add(function():void
         {
            _g.§\tTU(\x02§(true);
         },0);
         §y+§.§t#§.__add(function():void
         {
            300;
         },300);
         §y+§.§t#§.__add(function():void
         {
            _g.§Er\x133§({
               "x":_g.MV,
               "j\x01":_g.§j\x16§ + 8
            },null);
         },0);
         §y+§.§t#§.__add(function():void
         {
            null;
         },0,"helperMove");
         §y+§.§t#§.__add(function():void
         {
            _g.§\tTU(\x02§(false);
         },0);
      }
      
      public function init(param1:MovieClip) : void
      {
         if(§R\x12§ != null)
         {
            §R\x12§.parent.removeChild(§R\x12§);
         }
         §R\x12§ = param1;
         §\x10\x13L'\x01§.addChild(§R\x12§);
         §R\x12§.y = Number(§R\x12§.y + 23);
         §R\x12§.scaleX = -1;
         §i`2g§("walk");
         var _loc2_:* = §9^ \x1a\x01§.§RV)B\x03§(§\x1bJV%§);
         var _loc3_:String = §\x1c@§.§r$W\x02\x01§("HelperTip",{
            "_name":_loc2_.MErd,
            "_desc":_loc2_.§n+R\x11§
         });
         §O\bD\x15§(int(§N(Uu\x01§.x),int(§N(Uu\x01§.§j\x01§),11,_loc3_);
      }
      
      override public function §Er\x133§(param1:Object, param2:Object = undefined) : void
      {
         if(param2 == null)
         {
            param2 = 1;
         }
         while(int(param1.x) > 0 && !§\x1a'='\x02§(int(param1.x),int(param1.§j\x01§)))
         {
            param1.x = int(param1.x) - 1;
         }
         while(int(param1.x) < §*n8N\x03§.§?Q(_§ && !§\x1a'='\x02§(int(param1.x),int(param1.§j\x01§)))
         {
            param1.x = int(param1.x) + 1;
         }
         Log.trace(int(param1.x) + "," + int(param1.§j\x01§) + " " + Std.string(Boolean(§\x1a'='\x02§(int(param1.x),int(param1.§j\x01§)))),{
            "fileName":"Helper.hx",
            "}O;\x02":85,
            "className":"iso.Helper",
            "methodName":"goto"
         });
         §i`2g§("walk");
         super.§Er\x133§(param1,param2);
      }
      
      override public function §j2\x05\t\x03§() : Object
      {
         return §y+§.§\b#uG§.§ozEu\x02§(MV,§j\x16§);
      }
      
      public function §\x1a'='\x02§(param1:int, param2:int) : Boolean
      {
         var _loc3_:§\x13\x1eDd§ = §y+§.aZ9;
         var _loc4_:int = param1 + §*n8N\x03§.§' T\x0e\x01§;
         var _loc5_:int = param2 + §*n8N\x03§.§' T\x0e\x01§;
         return !(_loc4_ < 0 || _loc4_ >= _loc3_.§\x02\x187§ || _loc5_ < 0 || _loc5_ >= _loc3_.§ow\x02\x01§?true:Boolean(_loc3_.§6\b3U\x03§[_loc4_][_loc5_]));
      }
      
      public function §TI*\x19§() : void
      {
         var _g:Helper = this;
         §y+§.§t#§.__beginNewQueue();
         §y+§.§t#§.__add(function():void
         {
            _g.§`Y@S\x03§(null,int(§*n8N\x03§.§\x1f@\x071§.x) - 3,int(§*n8N\x03§.§\x1f@\x071§.§j\x01§) - 12);
         },0);
         §y+§.§t#§.__add(function():void
         {
            _g.§Er\x133§({
               "x":int(§*n8N\x03§.§\x1f@\x071§.x) - 2,
               "j\x01":int(§*n8N\x03§.§\x1f@\x071§.§j\x01§)
            },null);
         },0);
         §y+§.§t#§.__add(function():void
         {
            null;
         },0,"helperMove");
         §y+§.§t#§.__add(function():void
         {
            _g.§\tTU(\x02§(false);
         },0);
         §y+§.§t#§.__add(function():void
         {
            _g.§y+§.§t#§.__addParallel(function():void
            {
               _g.§y+§.§QF$\x1d\x01§();
            },100);
         },0);
         §y+§.§t#§.__add(function():void
         {
            300;
         },300);
         §y+§.§t#§.__add(function():void
         {
            _g.§`Y@S\x03§(null,int(§*n8N\x03§.§\x1f@\x071§.x),int(§*n8N\x03§.§\x1f@\x071§.§j\x01§));
         },0);
         §y+§.§t#§.__add(function():void
         {
            _g.§\tTU(\x02§(true);
         },0);
         §y+§.§t#§.__add(function():void
         {
            100;
         },100);
         §y+§.§t#§.__add(function():void
         {
            _g.§y+§.§t#§.__addParallel(function():void
            {
               _g.§y+§.§p;\x04 \x01§();
            },300);
         },0);
         §y+§.§t#§.__add(function():void
         {
            _g.§Er\x133§({
               "x":int(_g.§Q8\x05;\x02§.MV),
               "j\x01":int(_g.§Q8\x05;\x02§.§j\x16§)
            },null);
         },0);
         §y+§.§t#§.__add(function():void
         {
            null;
         },0,"helperMove");
         §y+§.§t#§.__add(function():void
         {
            _g.§i`2g§();
         },0);
         §y+§.§t#§.__add(function():void
         {
            _g.§Q]L?\x01§(int(_g.§Q8\x05;\x02§.Oxf),0);
         },0);
         §y+§.§t#§.__add(function():void
         {
            _g.§T\x10gT§ = true;
         },0);
      }
   }
}
